/*
This section actually builds the menuitem onto each menu choice.
*/

import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenuItem;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.event.ActionListener;

public class MyJFrame extends JFrame
{
	// To make compiler stop complaining about serializable classes
	private static final long serialVersionUID = 42L;
	
	JMenuBar menubar;
	JMenu f;
	JMenuItem mi;
	JCheckBoxMenuItem cb;

	public MyJFrame(String title)
	{
		super(title);

		// The menubar is invisible
		menubar = new JMenuBar();
		setJMenuBar(menubar);

		buildMenu();
		
		setBounds(100,100,500,400);
		setVisible(true);
		
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent we) {
				Dialog.display("COP 3337","The window is closing");
				dispose();
			}
		});
	}
	
	void buildMenu() {
		
		ActionListener actions = new Actions();
		
		// Build and add each menu choice onto the menubar
		for (int i = 0; i < Constants.MENU.length; i++) {
			f = new JMenu(Constants.MENU[i]);
			switch(i) {
				case 0: // Add menu FILE_MENU
					for (int j = 0; j < Constants.FILE_MENU.length; j++) {
						if (Constants.FILE_MENU[j] == null) {
							f.addSeparator();
						}
						else {
							f.add(mi = new JMenuItem(Constants.FILE_MENU[j]));
							mi.addActionListener(actions);
						}
					}
				break;

				case 1:
					for (int k = 0; k < Constants.TOOL_MENU.length; k++) {
						if (Constants.TOOL_MENU[k].equals("Edit")) {
							f.addSeparator();
							
							// Cascading menu
							JMenu m = new JMenu(Constants.TOOL_MENU[k]); 
							for (int l = 0; l < Constants.EDIT_MENU.length; l++) {
								m.add(cb = new JCheckBoxMenuItem(Constants.EDIT_MENU[l]));
								cb.addActionListener(actions);
							}
							
							f.add(m);
						}
						else {
							f.add(cb = new JCheckBoxMenuItem(Constants.TOOL_MENU[k]));
							cb.addActionListener(actions);
						}
					}
				break;
				/*	case 2:
				// Pattern is the same
				break;
				*/
			} // menubar switch
			menubar.add(f);
		} // menubar loop
	} // buildMenu() method
} // class