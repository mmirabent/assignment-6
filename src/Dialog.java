import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JFileChooser;
import java.io.File;

public final class Dialog {
	public static void displayErr(String s, String msg)
	{
		JOptionPane.showMessageDialog(null, msg, s, JOptionPane.ERROR_MESSAGE);
	}
	
	 public static void display(String s, String msg)
	{
		JOptionPane.showMessageDialog(null, msg, s, JOptionPane.INFORMATION_MESSAGE);
	}
         
        public static int getInt(String s)
         {
             return Integer.parseInt(JOptionPane.showInputDialog(null,s,"",JOptionPane.QUESTION_MESSAGE));
         }
        
        static String getWord(String s)
	{
		return JOptionPane.showInputDialog(s);
	}
         
        public static void display(String title, String text, int row, int col)
	{
		JTextArea s = new JTextArea(text, row, col);
		JScrollPane pane = new JScrollPane(s);
		JOptionPane.showMessageDialog(null, pane, title, JOptionPane.INFORMATION_MESSAGE);
	}
	
	public static File openFile(String title, String msg)
	{
		Dialog.display(title, msg);
		
		JFileChooser choose = new JFileChooser(".");
		int status = choose.showOpenDialog(null);
			
		if (status != JFileChooser.APPROVE_OPTION)
			return null;
		else
			return choose.getSelectedFile();
	}
	
	public static File saveFile(String title, String msg)
	{
		Dialog.display(title, msg);
		
		JFileChooser choose = new JFileChooser(".");
		int status = choose.showSaveDialog(null);
			
		if (status != JFileChooser.APPROVE_OPTION)
			return null;
		else
			return choose.getSelectedFile();
	}
}