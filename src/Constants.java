import java.awt.Rectangle;

public interface Constants {
	public static int 		WINDOW_X = 100;
	public static int		WINDOW_Y = 200;
	public static int		WINDOW_WIDTH = 450;
	public static int		WINDOW_HEIGHT = 400;
	public static Rectangle WINDOW_BOUNDS = 
		new Rectangle(WINDOW_X, WINDOW_Y, WINDOW_WIDTH, WINDOW_HEIGHT);
	
	public static String[] MENU = {
		"File",
		"Tool",
		"Help"
	};
	
	public static String[] FILE_MENU = {
		"New",
		"Open",
		null,
		"Save",
		null,
		"Close"
	};
	
	public static String[] TOOL_MENU = {
		"Sort",
		"Search",
		"Edit"
	};
	
	public static String[] EDIT_MENU = {
		"Copy",
		"Paste"
	};
	
	public static String[] BUTTONS = {
		"Write",
		"Draw",
		"kbIcon_med.png",
		"Close",
		"Browser"
	};
	
	public static String INITIAL_WEBPAGE = "http://www.google.com";
}