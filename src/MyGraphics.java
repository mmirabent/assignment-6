import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class MyGraphics extends JFrame
{
	// To make compiler stop complaining about serializable classes
	private static final long serialVersionUID = 42L;
	
	protected int lastX, lastY;


  	public MyGraphics()
  	{
	  	super("Smile white board ...");

	  	lastX=0;
	  	lastY=0;
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
	  	Container c = getContentPane();
		c.add(new whiteBoardPanel());
		
	    setBounds(100, 200, 850, 600);
		setBackground(Color.WHITE);
		
	    setVisible(true);
  	}
	protected void record(int x, int y)
	{
		lastX = x;
		lastY = y;
	}
	
  // Record position that mouse entered window or
  // where user pressed mouse button.
	private class PositionRecorder extends MouseAdapter
	{
		public void mouseEntered(MouseEvent e)
		{
		     record(e.getX(), e.getY());
		}
		public void mousePressed(MouseEvent e)
		{
		  record(e.getX(), e.getY());
		}
	}
  // As user drags mouse, connect subsequent positions
  // with short line segments.
	private class LineDrawer extends MouseMotionAdapter {
		public void mouseDragged(MouseEvent e) {
			int x = e.getX();
			int y = e.getY();

			Graphics g = getGraphics();
			g.setColor(Color.GREEN);
			g.fillOval(lastX, lastY, x/40, y/40);
			g.setColor(Color.BLUE);
			g.drawOval(lastX, lastY, x/40, y/40);

			record(x, y);
		}
	}
	
	private class whiteBoardPanel extends JPanel {
		// To make compiler stop complaining about serializable classes
		private static final long serialVersionUID = 42L;
		
		public whiteBoardPanel() {
			super();
			
		    addMouseListener(new PositionRecorder());
		    addMouseMotionListener(new LineDrawer());
		}
		
		@Override
		public void paintComponent(Graphics g) {
			// Logic to draw a star
			// Points for the 10 vertices calculated with WolframAlpha
			int xStarPoints[] = { -100,-24,-0,24,100,38,62,-0,-62,-38 };
			int yStarPoints[] = { -32,-32,-105,-32,-32,12,85,40,85,12 };
			
			// Make a new Polygon from the points above and move it in place
			Polygon star = new Polygon(xStarPoints, yStarPoints, 10);
			star.translate(120, 130);
			
			// set the color and fill baby fill
			g.setColor(Color.YELLOW);
			g.fillPolygon(star);
			
			
			// set points for the "house" polygon
			int xHousePoints[] = { 50,350,350,200,50 };
			int yHousePoints[] = { 250,250,150,50,150 };
			Polygon house = new Polygon(xHousePoints, yHousePoints, 5);
			house.translate(200,150);
			
			g.setColor(Color.MAGENTA);
			g.fillPolygon(house);
			
			// Fill the door with white
			// Rectangle door = new Rectangle(25,75);
			// door.translate(375,338);
			
			g.setColor(Color.WHITE);
			g.fillRect(375,325,50,75);
			
			// Logic to draw the text
			g.setColor(new Color(100, 150, 40));
			g.setFont(new Font("Futura", Font.BOLD, 48));
			g.drawString("Casa Mirabent", 240, 450);
		}
	}
}