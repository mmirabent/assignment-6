import javax.swing.*;
import java.awt.*;
import java.io.*;
import javax.swing.text.html.HTMLEditorKit;
import java.net.URL;

public class Browser {
	public Browser() {
		MyJFrame mjf = new MyJFrame("My web browser");
		Container c = mjf.getContentPane();
		JTextField urlField = new JTextField("http://");
		JEditorPane webContent = new JEditorPane();
		JScrollPane webScrollPane = new JScrollPane(webContent);
		
		mjf.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		mjf.setVisible(true);
		
		c.setLayout(new BorderLayout());
		c.add(urlField, BorderLayout.NORTH);
		
		try {
			webContent.setEditorKit(new HTMLEditorKit());
			webContent.setEditable(false);
			webContent.setPage(Constants.INITIAL_WEBPAGE);
			
			c.add(webScrollPane, BorderLayout.CENTER);
		}
		catch (Exception e) {
			System.out.println("Something Bad(tm) happened\n" + e.toString());
		}
	}
}