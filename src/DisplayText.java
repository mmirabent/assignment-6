import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import java.awt.Container;

public class DisplayText   
{
	private static JTextArea text;
	private static String buffer;

	public DisplayText( String title, String info) {
		MyJFrame f = new MyJFrame(title);
		Container c = f.getContentPane();

		text = new JTextArea(info); 

		JScrollPane sp = new JScrollPane(text);
		c.add( sp );

		f.setBounds(100,200, 500, 400 );
		f.setVisible(true);
	}
	
	public static void copy() {
		try {
			buffer = text.getSelectedText();
		}
		catch (NullPointerException e) {
			// There aren't any DisplayText objects initialized yet
		}
	}
	
	public static void paste() {
		try {
			text.insert(buffer,text.getCaretPosition());
		}
		catch (NullPointerException e) {
			// There aren't any DisplayText objects initialized yet
		}
	}
	
}