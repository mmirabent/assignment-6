
/* Demo  

This class adds buttons and images to the GUI. We make use of the GridBagLayout, 

GridBagConstraints, Insets, and Image classes from awt to lay out each item

From the swing package, JButton and ImageIcon

*/ 

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import java.awt.Container;

import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;

public class MyJButton extends JButton
{
	// To make compiler stop complaining about serializable classes
	private static final long serialVersionUID = 42L;
	
	MyJFrame f;

	MyJButton(MyJFrame f)
	{
		this.f = f;
		addButtons();
		f.pack();
	} 

	public void addButtons()
	{
		JButton b;

		Container c = f.getContentPane();

		// Places components in a grid of rows & columns
		GridBagLayout gbag = new GridBagLayout(); 

		// Specify the constraints for each component
		GridBagConstraints constraints = new GridBagConstraints();

		c.setLayout(gbag); // Layout each component

		for (int i = 0; i < Constants.BUTTONS.length; i++) {
			b = new JButton(Constants.BUTTONS[i]);
			
			switch(i) {
				case 0:
				// Specify the (x,y) coordinate for this component
				constraints.gridx = 0; 
				constraints.gridy = 0; // (x,y) = (0,0)
				constraints.insets = new Insets(20,110,10,0);
				break;

				case 1:
				constraints.gridx = 1;
				constraints.gridy = 0;	
				break;

				case 2:
				b = new JButton(new ImageIcon(Constants.BUTTONS[i]));
				constraints.gridx = 0;
				constraints.gridy = 1; 
				constraints.insets = new Insets(10,20,50,0);  
				break;
				
				case 3:
				constraints.gridx = 1;
				constraints.gridy = 1;
				break;
				
				case 4:
				constraints.gridx = 1;
				constraints.gridy = 2;
				break;
			}
			gbag.setConstraints(b, constraints);
			c.add(b);
			b.addActionListener(new Actions());
		}
	}   
}