import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.StreamTokenizer;
import java.io.BufferedWriter;
import java.io.FileWriter;


public class BasicFile
{
	private File file, backupFile;
	private String contentsOfPath;
	private int words, lines, chars;
	private BufferedWriter out;
	
	public BasicFile()
	{
		contentsOfPath = "";
		words = 0;
		lines = 1;
		chars = 0;
	}
	
	// ask for an input file
	public void selectFile() throws IOException
	{
		file = Dialog.openFile("Input File", "Please select the input file");
		if (file == null) throw new IOException();
	}
	
	// the IO exception this method throws comes from the creation of the
	// streams
	protected static void copyFiles(File from, File to) throws IOException
	{

		FileInputStream fis = new FileInputStream(from);
		BufferedInputStream bis = new BufferedInputStream(fis);
		DataInputStream dis = new DataInputStream(bis);
		
		FileOutputStream fos = new FileOutputStream(to);
		BufferedOutputStream bos = new BufferedOutputStream(fos);
		DataOutputStream dos = new DataOutputStream(bos);
		
		// The while loop will run until the EOF is encountered, the catch
		// statement safely does nothing, and finally, closes the files
		try {
			while(true)
				dos.writeByte(dis.readByte());
		} catch (EOFException e) {
			// Do nothing, we have reached the end of the file
		}
		
		dis.close();
		dos.close();
		
	}
	
	// I wrote this before reading the instructions. In testing, buffered
	// stream copys are orders of magnitude faster (100x faster compared to
	// unbuffered DataInputStreams and 10x faster compared to buffered
	// DataInputStreams such as the one above).
	protected static void copyFilesBuffered(File f, File tempFile) throws IOException
	{
		int i = 0;
		int bytesRead;
			
		// set our buffer size
		int chunkSz = 1024;
		
		byte[] b = new byte[chunkSz];
		
		// create the streams
		FileInputStream fis = new FileInputStream(f);
		BufferedInputStream bis = new BufferedInputStream(fis, chunkSz);
		
		FileOutputStream fos = new FileOutputStream(tempFile);
		BufferedOutputStream bos = new BufferedOutputStream(fos, chunkSz);
		
		// perform the copy
		while((bytesRead = bis.read(b, 0, chunkSz)) != -1)
			bos.write(b, 0, bytesRead);
		
		bis.close();
		bos.close();
	}
	
	
	public String getAbsolutePath()
	{
		return file.getAbsolutePath();
	}
	
	public String getSubdirs()
	{
		contentsOfPath = "";
		walk(file.getParentFile());
		return contentsOfPath;
	}
	
	private void walk(File f)
	{
		if(f.isFile()) {
			contentsOfPath += f.getName() + " is a file\n";
		} else {
			contentsOfPath += f.getName() + " is a directory\n";
			File [] subFiles = f.listFiles();
			
			if (subFiles != null) {
				for(File fi : subFiles)
				walk(fi);
			}
		}
	}
	
	public long getSize()
	{
		return file.length();
	}

	public String getName() {
		return file.getName();
	}
	
	
	public String getContent()
	{
		StringBuilder contents = new StringBuilder("");
		try {
			int ttype;
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			String line;
			
			while((line = br.readLine()) != null) {
				contents.append(line);
				contents.append("\n");
			}
		}
		catch (IOException e) {
			
		}
		return contents.toString();
	}
        
	public String search(String s) throws NullPointerException, FileNotFoundException, IOException
	{
		String results = "----- Search results for \"" + s + "\" -----\n";
		String currentLine;
		
		FileReader fr = new FileReader(file);
		LineNumberReader lnr = new LineNumberReader(fr);
                
		while((currentLine = lnr.readLine()) != null) {
			if(currentLine.toLowerCase().contains(s.toLowerCase()))
				results += lnr.getLineNumber() + ": " + currentLine + "\n";
		}
		
		return results;
	}
	
	public void close()
	{
		try {
			out.close();
		} catch (IOException e) {
			Dialog.displayErr("Error", "Unexpected error " + e + " has occurred and prevented closing the file, the output file may not be complete");
		}
	}
}